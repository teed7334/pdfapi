package com.teed7334.pdfapi.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import io.swagger.annotations.*;
import io.github.cdimascio.dotenv.Dotenv;
import io.ipfs.multibase.Base58;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.UnrecoverableKeyException;
import java.security.GeneralSecurityException;

import com.teed7334.pdfapi.dto.PDFDto;
import com.teed7334.pdfapi.dto.PKCSDto;
import com.teed7334.pdfapi.dto.SignDto;
import com.teed7334.pdfapi.dto.UploadDto;
import com.teed7334.pdfapi.dto.IPFSDto;
import com.teed7334.pdfapi.dto.FileDto;
import com.teed7334.pdfapi.dto.SignInfoDto;
import com.teed7334.pdfapi.dto.ReadSignDto;
import com.teed7334.pdfapi.dto.PDFCertDto;
import com.teed7334.pdfapi.dto.PDFCertDataDto;
import com.teed7334.pdfapi.dto.SaveAsDto;
import com.teed7334.pdfapi.dto.ResultObject;

import com.teed7334.pdfapi.helper.PDF;
import com.teed7334.pdfapi.helper.SHA;
import com.teed7334.pdfapi.helper.PKCS;
import com.teed7334.pdfapi.helper.Hash;
import com.teed7334.pdfapi.helper.IPFServer;
import com.teed7334.pdfapi.helper.Storage;

@Api(tags = "PDF API")
@RestController
@RequestMapping(value = "/api")
public class HomeController {

	@ApiOperation("計算檔案指紋碼")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "計算完成")})
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/v1/calcFinger", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResultObject calcFinger(@RequestBody FileDto fileDto) throws IOException
    {
        ResultObject ro = new ResultObject();
        File file = new File(fileDto.getSrc());
        String hash = Base58.encode(Hash.SHA256.checksum(file));
        ro.setCode(1);
        ro.setMessage(hash);
        return ro;
    }
	
    @ApiOperation("新增PDF証書")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "存檔成功")})
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/v1/createCert", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResultObject createCert(@RequestBody PDFCertDto pdfCertDto) throws IOException, URISyntaxException
    {
        ResultObject ro = new ResultObject();
        PDF pdf = new PDF();
        SaveAsDto saveAs = new SaveAsDto();
        Storage storage = new Storage();
        String fontPath = storage.getFont();
        String targetPath = storage.getSource();
        String dest = targetPath + "/" + this.makeFileName() + ".pdf";
        saveAs.setFont(fontPath);
        saveAs.setPath(dest);
        String certImage = storage.getCertImage();
        String backgroundImage = storage.getBackgroundImage();
        String iconPath = storage.getIcon(pdfCertDto.getCertification());
        PDFCertDataDto pdfCertDataDto = new PDFCertDataDto();
        pdfCertDataDto.setCertImage(certImage);
        pdfCertDataDto.setBackgroundImage(backgroundImage);
        pdfCertDataDto.setLogo(iconPath);
        pdf.createCert(pdfCertDto, pdfCertDataDto, saveAs);
        ro.setCode(1);
        ro.setMessage(saveAs.getPath());
        return ro;
    }

    @ApiOperation("新增純文字PDF")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "存檔成功")})
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/v1/createTextOnly", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResultObject createTextOnly(@RequestBody PDFDto pdfDTO) throws IOException, URISyntaxException 
    {
        ResultObject ro = new ResultObject();
        PDF pdf = new PDF();
        SaveAsDto saveAs = new SaveAsDto();
        Storage storage = new Storage();
        String fontPath = storage.getFont();
        String targetPath = storage.getSource();
        String dest = targetPath + "/" + this.makeFileName() + ".pdf";
        saveAs.setFont(fontPath);
        saveAs.setPath(dest);
        pdf.createTextOnly(pdfDTO, saveAs);
        ro.setCode(1);
        ro.setMessage(saveAs.getPath());
        return ro;
    }

    @ApiOperation("加簽PDF")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "加簽成功")})
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/v1/sign", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResultObject sign(@RequestBody SignDto signDTO) throws IOException, KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, GeneralSecurityException, URISyntaxException 
    {
        ResultObject ro = new ResultObject();
        PDF pdf = new PDF();

        SignInfoDto info = pdf.readSign(signDTO.getSrc());
        if (info != null) {
            ro.setCode(0);
            ro.setMessage("文件已加簽");
            return ro;
        }

        Storage storage = new Storage();
        String targetPath = storage.getSign();
        String dest = targetPath + "/" + this.makeFileName() + ".pdf";

        PKCS pkcs = new PKCS();
        pkcs.open();
        PKCSDto pkcsDto = this.bindPKCSDto(signDTO.getSrc(), dest, pkcs);
        pdf.sign(pkcsDto, signDTO);
        pkcs.close();

        ro.setCode(1);
        ro.setMessage(dest);
        return ro;
    }

    @ApiOperation("取得加簽資訊")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "加簽資訊取得成功")})
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/v1/readSign", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SignInfoDto readSign(@RequestBody ReadSignDto readSignDto) throws GeneralSecurityException, IOException
    {
        PDF pdf = new PDF();
        SignInfoDto dto = pdf.readSign(readSignDto.getSrc());
        if (dto == null) {
            SignInfoDto info = new SignInfoDto();
            return info;
        }
        return dto;
    }

    @ApiOperation("PDF上鏈")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "上鏈成功")})
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/v1/upload", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public IPFSDto upload(@RequestBody UploadDto uploadDto) throws IOException
    {
        IPFServer ipfs = new IPFServer();
        String hash = ipfs.upload(uploadDto.getSrc());
        String url = "https://ipfs.io/ipfs/" + hash;

        IPFSDto ipfsDto = new IPFSDto();
        ipfsDto.setHash(hash);
        ipfsDto.setUrl(url);
        return ipfsDto;
    }

    protected PKCSDto bindPKCSDto(String src, String dest, PKCS pkcs) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, GeneralSecurityException
    {
        PKCSDto dto = new PKCSDto();
        dto.setSrc(src);
        dto.setDest(dest);

        dto.setChain(pkcs.getChain());
        dto.setCert(pkcs.getCert());
        dto.setPrivateKey(pkcs.getPrivateKey());
        dto.setProvider(pkcs.getProvider());
        Dotenv env = Dotenv.load();
        dto.setReason(env.get("reason"));
        dto.setLocation(env.get("location"));
        return dto;
    }

    protected String makeFileName() {
        long now = System.currentTimeMillis();
        Dotenv env = Dotenv.load();
        String inputValue = Long.toString(now) + env.get("salt");
        String hash = SHA.getSHA256(inputValue);
        return hash;
    }
}