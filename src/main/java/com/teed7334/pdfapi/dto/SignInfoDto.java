package com.teed7334.pdfapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "取得簽章資訊")
public class SignInfoDto {
    @ApiModelProperty(value = "簽章日期", required = true)
    private String signTime;
    @ApiModelProperty(value = "加密標準", required = true)
    private String cryptoStandard;
    @ApiModelProperty(value = "簽章地點", required = true)
    private String location;
    @ApiModelProperty(value = "簽章原因", required = true)
    private String reason;
    @ApiModelProperty(value = "簽章內容", required = true)
    private String content;
}