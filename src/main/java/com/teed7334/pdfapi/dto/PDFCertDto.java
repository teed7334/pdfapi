package com.teed7334.pdfapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "証書資訊")
public class PDFCertDto {

    @ApiModelProperty(value = "存証申請人", required = true)
    private String account;

    @ApiModelProperty(value = "檔案路徑", required = true)
    private String path;

    @ApiModelProperty(value = "認証單位 enum [globalsign, chunghwa]", required = true)
    private String certification;
    
    @ApiModelProperty(value = "檔案指紋碼", required = true)
    private String fingerCode;

    @ApiModelProperty(value = "檔案名稱", required = true)
    private String fileName;

    @ApiModelProperty(value = "檔案說明", required = true)
    private String fileDescription;
}