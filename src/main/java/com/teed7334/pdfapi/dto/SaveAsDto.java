package com.teed7334.pdfapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "PDF另存新檔資訊")
public class SaveAsDto {
	@ApiModelProperty(value = "字型", required = true)
    private String font;
	
	@ApiModelProperty(value = "路徑", required = true)
    private String path;
}
