package com.teed7334.pdfapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "上鏈資訊")
public class FileDto {
    @ApiModelProperty(value = "檔案路徑", required = true)
    private String src;
}