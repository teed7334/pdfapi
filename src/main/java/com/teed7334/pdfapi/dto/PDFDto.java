package com.teed7334.pdfapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "PDF資料")
public class PDFDto {
    @ApiModelProperty(value = "內文", required = true)
    private String[] content;
}