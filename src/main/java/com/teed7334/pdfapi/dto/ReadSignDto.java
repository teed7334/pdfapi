package com.teed7334.pdfapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "簽章資訊")
public class ReadSignDto {
    @ApiModelProperty(value = "來源PDF路徑", required = true)
    private String src;
}