package com.teed7334.pdfapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "簽章資訊")
public class SignDto {
    @ApiModelProperty(value = "來源PDF路徑", required = true)
    private String src;

    @ApiModelProperty(value = "簽章寬度", required = true)
    private int width;

    @ApiModelProperty(value = "簽章高度", required = true)
    private int height;

    @ApiModelProperty(value = "簽章X軸", required = true)
    private int x;

    @ApiModelProperty(value = "簽章Y軸", required = true)
    private int y;
}