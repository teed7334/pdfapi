package com.teed7334.pdfapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "上鏈資訊")
public class IPFSDto {
    @ApiModelProperty(value = "檔案Hash值", required = true)
    private String hash;

    @ApiModelProperty(value = "IPFS路徑", required = true)
    private String url;
}