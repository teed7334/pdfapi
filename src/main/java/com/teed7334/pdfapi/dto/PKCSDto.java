package com.teed7334.pdfapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

@Data
@ApiModel(description = "簽章資訊")
public class PKCSDto {
    
    @ApiModelProperty(value = "來源PDF路徑", required = true)
    private String src;

    @ApiModelProperty(value = "寫入PDF路徑", required = true)
    private String dest; 

    @ApiModelProperty(value = "X509資訊", required = true)
    private X509Certificate cert;
    
    @ApiModelProperty(value = "憑證資訊", required = true)
    private Certificate[] chain;

    @ApiModelProperty(value = "私鑰", required = true)
    private PrivateKey privateKey; 

    @ApiModelProperty(value = "HSM", required = true)
    private String provider;

    @ApiModelProperty(value = "簽約原因", required = true)
    private String reason;

    @ApiModelProperty(value = "簽約位置", required = true)
    private String location;
}