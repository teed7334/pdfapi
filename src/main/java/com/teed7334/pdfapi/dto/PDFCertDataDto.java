package com.teed7334.pdfapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "証書資訊")
public class PDFCertDataDto {
    @ApiModelProperty(value = "認証底圖", required = true)
    private String certImage;
    
    @ApiModelProperty(value = "背景圖", required = true)
    private String backgroundImage;
    
    @ApiModelProperty(value = "認証單位Icon", required = true)
    private String logo;
}
