package com.teed7334.pdfapi.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "結果回傳物件")
public class ResultObject {
    @ApiModelProperty(value = "狀態代碼", required = true)
    private int code;

    @ApiModelProperty(value = "狀態訊息", required = true)
    private String message;
}