package com.teed7334.pdfapi.helper;

import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.github.cdimascio.dotenv.Dotenv;
import java.io.File;

import java.io.IOException;

public class IPFServer {
    public String upload(String path) throws IOException
    {
        Dotenv env = Dotenv.load();
        String url = env.get("ipfsUrl");
        String port = env.get("ipfsPort");
        String ipfsPath = "/ip4/" + url + "/tcp/" + port;
        IPFS ipfs = new IPFS(ipfsPath);
        NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(new File(path));
        MerkleNode addResult = ipfs.add(file).get(0);
        String hash = addResult.hash.toString();
        ipfs.pin.add(addResult.hash);
        return hash;
    }
}
    