package com.teed7334.pdfapi.helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Storage {
	
	private final String font = "/fonts/NotoSansCJKtc-Regular.otf";
	private final String backgroundImage = "/images/background.png";
    private final String certImage = "/images/cert.png";
    
    private final String globalSign = "/images/globalsign.png";
    private final String chunghwa = "/images/chunghwa.png";
    
    public String getSource() throws URISyntaxException {
    	String path = this.getTarget() + "/files/source";
    	return path;
    }
    
    public String getSign() throws URISyntaxException {
    	String path = this.getTarget() + "/files/sign";
    	return path;
    }
    
	public String getFont() throws IOException {
		return this.getFile(this.font);
	}
	
	public String getCertImage() throws IOException {
		return this.getFile(this.certImage);
	}
	
	public String getBackgroundImage() throws IOException {
		return this.getFile(this.backgroundImage);
	}
	
	public String getIcon(String logoName) throws IOException {
		String icon = "";
        switch(logoName) {
            case "globalsign":
                icon = this.getFile(this.globalSign);
                break;
            case "chunghwa":
                icon = this.getFile(this.chunghwa);
                break;
            default:
                break;
        }
        return icon;
	}
	
	private String getFile(String filePath) throws IOException {
		Resource resource = new ClassPathResource(filePath);
		String[] arr = filePath.split("/");
		String tmpFile = arr[arr.length -1];
		String tempPath = System.getProperty("java.io.tmpdir") + "/" + tmpFile;
		File file = new File(tempPath);
		
		try {
			IOUtils.copy(resource.getInputStream(), new FileOutputStream(file));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String path = file.getPath();
		return path;
	}
	
	private String getTarget() {
    	String classPath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
    	String[] arr = classPath.split("/");
    	String path = arr[arr.length -2];
    	return path;
    }
}
