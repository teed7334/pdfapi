package com.teed7334.pdfapi.helper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

import sun.security.pkcs11.SunPKCS11;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.UnrecoverableKeyException;

import io.github.cdimascio.dotenv.Dotenv;

@SuppressWarnings("restriction")
public class PKCS {

    private Certificate[] chain;
    private X509Certificate cert;
    private PrivateKey privateKey;
    private SunPKCS11 pkcs11;
    
    public void open() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, GeneralSecurityException
    {
        Dotenv env = Dotenv.load();
        String settings = "name=" + env.get("hsmId") + "\n" + "library=" + env.get("libraryPath");
        byte[] settingsBytes = settings.getBytes();
        ByteArrayInputStream conf = new ByteArrayInputStream(settingsBytes);
        this.pkcs11 = new SunPKCS11(conf);
        Security.addProvider(this.pkcs11);
        char[] pin = env.get("pin").toCharArray();

        KeyStore ks = KeyStore.getInstance("PKCS11", this.pkcs11);
        ks.load(null, pin);
         
        Enumeration<String> enumeration = ks.aliases();
        
        String alias;
        String useKey = env.get("key");
        while(enumeration.hasMoreElements()) {
            alias = (String) enumeration.nextElement();
            if (alias.equals(useKey)) {
                this.cert = (X509Certificate) ks.getCertificate(alias);
                this.chain = ks.getCertificateChain(alias);
                this.privateKey = (PrivateKey) ks.getKey(alias, pin);
            }
        }
    }

    public String getProvider()
    {
        return this.pkcs11.getName();
    }

    public Certificate[] getChain() 
    {
        return this.chain;
    }

    public X509Certificate getCert()
    {
        return this.cert;
    }

    public PrivateKey getPrivateKey()
    {
        return this.privateKey;
    }

    public void close()
    {
        Security.removeProvider(this.pkcs11.getName());
    }
}