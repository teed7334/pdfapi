package com.teed7334.pdfapi.helper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.List;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.security.GeneralSecurityException;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.util.ASN1Dump;
import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDate;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.PdfString; 
import com.itextpdf.kernel.pdf.StampingProperties;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.AreaBreakType;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.signatures.BouncyCastleDigest;
import com.itextpdf.signatures.DigestAlgorithms;
import com.itextpdf.signatures.IExternalDigest;
import com.itextpdf.signatures.IExternalSignature;
import com.itextpdf.signatures.PdfSignatureAppearance;
import com.itextpdf.signatures.PdfSigner;
import com.itextpdf.signatures.PdfSignature;
import com.itextpdf.signatures.PrivateKeySignature;
import com.itextpdf.signatures.IOcspClient;
import com.itextpdf.signatures.OcspClientBouncyCastle;
import com.itextpdf.signatures.ITSAClient;
import com.itextpdf.signatures.TSAClientBouncyCastle;
import com.itextpdf.signatures.CertificateUtil;
import com.itextpdf.signatures.SignatureUtil;

import com.teed7334.pdfapi.dto.PDFDto;
import com.teed7334.pdfapi.dto.PDFCertDto;
import com.teed7334.pdfapi.dto.PDFCertDataDto;
import com.teed7334.pdfapi.dto.PKCSDto;
import com.teed7334.pdfapi.dto.SaveAsDto;
import com.teed7334.pdfapi.dto.SignDto;
import com.teed7334.pdfapi.dto.SignInfoDto;

public class PDF {

    private PdfDocument pdf;
    private PageSize pageSize = PageSize.A4;
    private Document doc;

    public void createCert(PDFCertDto pdfCertDto, PDFCertDataDto pdfCertDataDto, SaveAsDto saveAs) throws IOException 
    {
        this.pdf = new PdfDocument(new PdfWriter(saveAs.getPath()));
        pdf.setDefaultPageSize(this.pageSize);
        this.doc = new Document(this.pdf);
        this.makeCert(pdfCertDto, pdfCertDataDto, saveAs);
        this.doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
        this.makeAttach(pdfCertDto, pdfCertDataDto, saveAs);
        this.doc.close();
    }

    public void createTextOnly(PDFDto pdfDto, SaveAsDto saveAs) throws IOException 
    {
        this.pdf = new PdfDocument(new PdfWriter(saveAs.getPath()));
        this.pdf.setDefaultPageSize(this.pageSize);
        this.doc = new Document(this.pdf);
        PdfFont font = PdfFontFactory.createFont(saveAs.getFont(), PdfEncodings.IDENTITY_H, true);
        String[] arr = pdfDto.getContent();
        for (String str : arr) {
            this.doc.add(new Paragraph(str).setFont(font));
        }
        this.doc.close();
    }

    public void sign(PKCSDto pkcsDto, SignDto signDto) throws GeneralSecurityException, IOException 
    {
        PdfReader reader = new PdfReader(pkcsDto.getSrc());
        PdfSigner signer = new PdfSigner(reader, new FileOutputStream(pkcsDto.getDest()), new StampingProperties());

        Rectangle rect = new Rectangle((float) signDto.getX(), (float) signDto.getY(), (float) signDto.getWidth(), (float) signDto.getHeight());
        PdfSignatureAppearance appearance = signer.getSignatureAppearance();
        appearance
                .setReason(pkcsDto.getReason())
                .setLocation(pkcsDto.getLocation())
                .setReuseAppearance(false)
                .setPageRect(rect)
                .setPageNumber(1);
        signer.setFieldName("sig");

        IExternalSignature pks = new PrivateKeySignature(pkcsDto.getPrivateKey(), DigestAlgorithms.SHA256, pkcsDto.getProvider());
        IExternalDigest digest = new BouncyCastleDigest();

        IOcspClient ocsp = new OcspClientBouncyCastle(null);
        String tsaUrl = CertificateUtil.getTSAURL(pkcsDto.getCert());
        ITSAClient tsa = new TSAClientBouncyCastle(tsaUrl);

        if (tsaUrl == null) {
            tsa = null;
        }

        signer.signDetached(digest, pks, pkcsDto.getChain(), null, ocsp, tsa, 0, PdfSigner.CryptoStandard.CADES);
    }

    public SignInfoDto readSign(String path) throws GeneralSecurityException, IOException
    {
        SignInfoDto info = new SignInfoDto();
        PdfReader reader = new PdfReader(path);
        this.pdf = new PdfDocument(reader);
        SignatureUtil signUtil = new SignatureUtil(this.pdf);
        List<String> names = signUtil.getSignatureNames();

        if (names.isEmpty()) {
            return null;
        }
        
        String name = names.get(0);
        PdfSignature sign = signUtil.getSignature(name);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        PdfString date = sign.getDate();

        info.setSignTime(dateFormat.format(PdfDate.decode(date.getValue()).getTime()));
        info.setLocation(sign.getLocation());
        info.setReason(sign.getReason());
        info.setCryptoStandard(sign.getSubFilter().getValue());
        
        PdfString content = sign.getContents();
        byte[] contentBytes = PdfEncodings.convertToBytes(content.getValue(), null);
        ASN1InputStream din = new ASN1InputStream(new ByteArrayInputStream(contentBytes));
        ASN1Primitive pkcs = din.readObject();

        info.setContent(ASN1Dump.dumpAsString(pkcs));
        din.close();
        return info;
    }

    private boolean isHalfWidth(char c)   {
        return '\u0000' <= c && c <= '\u00FF'
            || '\uFF61' <= c && c <= '\uFFDC'
            || '\uFFE8' <= c && c <= '\uFFEE' ;

    }

    private int strLength(final String value) {
        int length = 0;
        String str = (value == null) ? "" : value;
        char character;
       
        for (int i = 0; i < str.length(); i++) {
            character = str.charAt(i);
            if (Character.UnicodeBlock.of(character) != Character.UnicodeBlock.BASIC_LATIN || !isHalfWidth(character)) {
                length++;
            }
           
            length++;
        }
       
        return length;
    }

    private void makeCert(PDFCertDto pdfCertDto, PDFCertDataDto pdfCertDataDto, SaveAsDto saveAs) throws IOException
    {
        PdfCanvas canvas = new PdfCanvas(this.pdf.addNewPage());
        canvas.addImage(ImageDataFactory.create(pdfCertDataDto.getCertImage()), this.pageSize, false);
        PdfFont font = PdfFontFactory.createFont(saveAs.getFont(), PdfEncodings.IDENTITY_H, true);
        
        Float x = (float) 80;
        Float y = (float) 502;
        Float width = this.pageSize.getWidth() - x - 80;
        this.doc.add(
            new Paragraph(pdfCertDto.getAccount())
                .setFont(font)
                .setTextAlignment(TextAlignment.CENTER)
                .setFontSize(16)
                .setFixedPosition(x, y, width));
        
        String hash = pdfCertDto.getFingerCode();
        x = (float) 80;
        y = this.pageSize.getHeight() / 2 - 32 + 16;
        this.doc.add(
            new Paragraph(hash)
                .setFont(font)
                .setTextAlignment(TextAlignment.LEFT)
                .setFontSize(16)
                .setFixedPosition(x, y, 600));
        
        String certLogo = pdfCertDataDto.getLogo();
        Image img = new Image(ImageDataFactory.create(certLogo));
        float imageX = this.pageSize.getWidth() / 4 * 3 - img.getImageScaledWidth() / 2;
        float imageY = this.pageSize.getHeight() / 5  - img.getImageScaledHeight() / 2 - 30;
        img.setFixedPosition(imageX, imageY);
        this.doc.add(img);
    }

    private void makeAttach(PDFCertDto pdfCertDto, PDFCertDataDto pdfCertDataDto, SaveAsDto saveAs) throws IOException
    {
        PdfCanvas canvas = new PdfCanvas(pdf.getLastPage());
        canvas.addImage(ImageDataFactory.create(pdfCertDataDto.getBackgroundImage()), this.pageSize, false);
        Image img = new Image(ImageDataFactory.create(pdfCertDto.getPath()));
        PdfFont font = PdfFontFactory.createFont(saveAs.getFont(), PdfEncodings.IDENTITY_H, true);
        
        Float x = this.pageSize.getWidth() / 2 + 20 - 8 * 12;
        Float y = this.pageSize.getHeight() / 5 * 4 + 16 - 1;
        Float width = (float) 300;
        this.doc.add(
            new Paragraph(pdfCertDto.getFileName())
                .setFont(font)
                .setTextAlignment(TextAlignment.LEFT)
                .setFontSize(16)
                .setFixedPosition(x, y, width));
        
        double lines =  Math.floor((double) this.strLength(pdfCertDto.getFileDescription()) / 18);
        if (lines < 1) {
        	lines = 1;
        } else if (lines >= 3) {
        	lines = 2;
        }
        y = this.pageSize.getHeight() / 5 * 4 - 80 + (3 - (int) lines) * 32 + 1;
        this.doc.add(
            new Paragraph(pdfCertDto.getFileDescription())
                .setFont(font)
                .setTextAlignment(TextAlignment.LEFT)
                .setFontSize(16)
                .setFixedPosition(x, y, width));
        
        x = this.pageSize.getWidth() / 2 - img.getImageScaledWidth() / 2;
        y = this.pageSize.getHeight() / 2 - img.getImageScaledHeight() / 2 - 100;
        img.setFixedPosition(x, y);
        this.doc.add(img);
        
        long fileSize = new File(pdfCertDto.getPath()).length();
        DecimalFormat formatter = new DecimalFormat("#,###");
        String fileSizeStr = formatter.format(fileSize) + " bytes";
        x = this.pageSize.getWidth() / 2 + 16;
        y = this.pageSize.getHeight() / 25 + 1;
        this.doc.add(
            new Paragraph(fileSizeStr)
                .setFont(font)
                .setTextAlignment(TextAlignment.LEFT)
                .setFontSize(16)
                .setFixedPosition(x, y, 300));
    }
}