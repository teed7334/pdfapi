# pdfapi

用來建立有數位簽章的PDF用

## 資料夾結構
./src/main/java/com/teed7334/pdfapi/controller 控制器

./src/main/java/com/teed7334/pdfapi/dto 資料傳輸物件

./src/main/java/com/teed7334/pdfapi/helper 函式庫

./src/main/resources/fonts 字型檔

./src/main/resources/application.properties 服務器啟用 Port

./src/main/java/com/teed7334/pdfapi/DemoApplication.java 主程式

./src/main/java/com/teed7334/pdfapi/SwaggerConfig.java Swagger 設定檔

./target/files/source PDF 原始檔

./target/files/sign 加簽後 PDF

## 資料流
User -> Borwser -> HTTP Request -> 主程式 -> 分配到相應之控制器 -> 初步檢查 HTTP Request 參數都有帶齊 -> 產生 PDF -> 讀取USB HSM -> PDF 加數位簽章 -> 回傳檔案位置 -> 輸出 HTTP Response -> Borwser -> User

## 使用說明
1. 將 .env.swp 置換成 .env
2. .env 內參數對應如下
```
reason="[加簽原因]"
location="[加簽地點]"
salt="[供 SHA-256 產生檔案名鹽值]"

libraryPath="[OpenSC Library 所在位置]"
hsmId="[HSM 唯一編號]"
pin="[PIN 碼]"
```
3. 編譯本程式，建議採用 Java 1.8 ，編譯後會產出 jar 檔在 ./target 資料夾中
```
mvn package
```

4. 啟動本程式，請將 ./target/files 等資料夾與 .env ，與運行的 jar 檔在同一個資料夾
```
java -jar pdfapi-0.0.1-SNAPSHOT.jar
```

5. API使用說明文件
```
http://[Your IP]:8081/swagger-ui.html
```

## Windows下運行的一些小細節
1. 於jar運行環境同目錄，需新增以下資料夾./BOOT-INF、./BOOT-INF/files、./BOOT-INF/files/source、./BOOT-INF/files/sign
2. 於Windows下運行請追加sunpkcs11 Debug參數
```
java -jar -Djava.security.debug=sunpkcs11 pdfapi-0.0.1-SNAPSHOT.jar
```
  